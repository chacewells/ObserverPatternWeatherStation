package com.wellsfromwales.weatherstation;

public interface DisplayElement {
	public void display();
}
