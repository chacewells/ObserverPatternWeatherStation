package com.wellsfromwales.weatherstation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {
	private ArrayList<Float> temperatures;
	private ArrayList<Float> humidities;
	private ArrayList<Float> pressures;
	private Observable observable;
	
	public StatisticsDisplay(Observable observable) {
		super();
		
		this.observable = observable;
		if (this.observable != null)
			this.observable.addObserver(this);
		
		temperatures = new ArrayList<Float>();
		humidities = new ArrayList<Float>();
		pressures = new ArrayList<Float>();
	}
	
	@Override
	public void display() {
		System.out.println("Statistics");
		for (Integer i : new int[40])
			System.out.print("-");
		System.out.println();
		int count = 0;
		for (ArrayList<Float> list : new ArrayList<ArrayList<Float>>(){{
			add(temperatures); add(humidities); add(pressures);	
		}}) 
		{
			String measurement;
			switch(count++){
			case 0:	measurement = "temperature";	break;
			case 1:	measurement = "humidity";		break;
			case 2:	measurement = "pressure";		break;
				default: measurement = "what?";
			}
			
			System.out.println(measurement);
			System.out.printf("\t%s: %.2f%n", "Min", getMin(list));
			System.out.printf("\t%s: %.2f%n", "Max", getMax(list));
			System.out.printf("\t%s: %.2f%n", "Avg", getAverage(list));
		}
	}
	
	private float getMin(ArrayList<Float> list) {
		@SuppressWarnings("unchecked")
		ArrayList<Float> sorted = (ArrayList<Float>)list.clone();
		Collections.sort(sorted);
		if ( sorted.size() > 0) 
			return sorted.get(0);
		else
			throw new IndexOutOfBoundsException("The list is empty, dummy");
	}
	
	private float getMax(ArrayList<Float> list) {
		@SuppressWarnings("unchecked")
		ArrayList<Float> sorted = (ArrayList<Float>)list.clone();
		Collections.sort(sorted);
		if ( sorted.size() > 0)
			return sorted.get( sorted.size() - 1 );
		else
			throw new IndexOutOfBoundsException("The list is empty");
	}
	
	private float getAverage(ArrayList<Float> list) {
		return (float)list.size() / getSum(list);
	}
	
	private float getSum(ArrayList<Float> list) {
		float sum = 0.0f;
		for (Float num : list)
			sum += num;
		return sum;
	}

	@Override
	public void update(Observable o, Object arg) {
		WeatherData weatherData = (WeatherData) o;
		temperatures.add( weatherData.getTemperature() );
		humidities.add( weatherData.getHumidity() );
		pressures.add( weatherData.getPressure() );
		display();		
	}

}
