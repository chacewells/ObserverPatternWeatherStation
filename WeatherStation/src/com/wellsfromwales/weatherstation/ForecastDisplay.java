package com.wellsfromwales.weatherstation;

import java.util.Observable;
import java.util.Observer;

public class ForecastDisplay implements Observer, DisplayElement {
	private float temperature;
	private Observable observable;
	
	public ForecastDisplay( Observable o ) {
		super();
		
		this.observable = o;
		if (this.observable != null)
			this.observable.addObserver(this);
	}

	@Override
	public void display() {
		System.out.println("Forecast: Not my problem, but it might be " 
				+ temperature);
	}

	@Override
	public void update(Observable o, Object arg) {
		WeatherData weatherData = (WeatherData) o;
		this.temperature = weatherData.getTemperature();
		display();
	}

}
